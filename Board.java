public class Board {
	private Square[][] tictactoeBoard;
	public Board() {
		this.tictactoeBoard = new Square[3][3];
		for(int i = 0; i < tictactoeBoard.length; i++) {
			for(int j = 0; j < tictactoeBoard[i].length; j++) {
				this.tictactoeBoard[i][j] = Square.BLANK;
			}	
		}
	}
	public String toString() {
		String x = "";
		for(int i = 0; i < tictactoeBoard.length; i++) {
			x += "\n";
			for(int j = 0; j < tictactoeBoard[i].length; j++) {
				 x += tictactoeBoard[i][j] + " ";	
			}	
		}
		return x;	
	}
	public boolean placeToken(int row, int col, Square playerToken) {
		if (row > 3 || row < 0 || col > 3 || col < 0) {
			return false;
		}
		if(tictactoeBoard[col][row] == Square.BLANK){
			tictactoeBoard[col][row] = playerToken;
			return true;
		}
		return false;
	}
	public boolean checkIfFull() {
		for(int i = 0; i < tictactoeBoard.length; i++) {
			for(int j = 0; j < tictactoeBoard[i].length; j++) {
				if(this.tictactoeBoard[i][j] == Square.BLANK) {
					return false;	
				}
			}	
		}	
		return true;
	}
	private boolean checkWinningHorizontal(Square playerToken) {
		for(int i=0; i < tictactoeBoard.length; i++) {
			for(int j=0; j < tictactoeBoard[i].length; j++) {
				if(this.tictactoeBoard[i][j] == playerToken && this.tictactoeBoard[i][tictactoeBoard[j].length - 2] == playerToken && this.tictactoeBoard[i][tictactoeBoard[j].length - 1] == playerToken){
					return true;	
				}
			}		
		}
		return false;	
	}
	private boolean checkWinningVertical(Square playerToken) {	
		for(int i=0; i < tictactoeBoard.length; i++) {
			for(int j=0; j < tictactoeBoard[i].length; j++) {	
				if(this.tictactoeBoard[i][j] == playerToken && this.tictactoeBoard[tictactoeBoard[i].length - 2][j] == playerToken && this.tictactoeBoard[tictactoeBoard[i].length -1][j] == playerToken) {
					return true;	
				}
			}
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken) {
		if(checkWinningVertical(playerToken) || checkWinningHorizontal(playerToken)) {
			return true;	
		}
		else {
			return false;	
		}
	}
}