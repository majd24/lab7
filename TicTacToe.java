import java.util.Scanner;
public class TicTacToe {
	public static void main(String[] args) {
		Scanner object = new Scanner(System.in);
		System.out.println("welcome to tic-tac-toe:: ");
		Board newBoard = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		while(!gameOver) {
			System.out.println(newBoard);
			if(player == 1) {
				playerToken = Square.X;
			}
			else {	
				playerToken = Square.O;
			}
			int row = 0;
			int col = 0;
			boolean x = true;
			while(x) {
				System.out.println("input col:: ");
				row = object.nextInt();
				System.out.println("input row:: ");
				col = object.nextInt();	
				if(!newBoard.placeToken(row, col, playerToken)) {
					System.out.println("play again!");
				}	
				else {
					x = false;		
				}
			}
			newBoard.placeToken(row, col, playerToken);
			if(newBoard.checkIfFull()) {	
				System.out.println("it's a tie!");
				gameOver = true;
			}
			else if(newBoard.checkIfWinning(playerToken)) {
				gameOver = true;	
			}	
			else {
				player += 1;
				if(player > 2) {
					player = 1;
				}	
			}
		}
	}
}